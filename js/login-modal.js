const loginLink = document.querySelector(".login-link");
const loginModal = document.querySelector(".modal-login");
const overlayModal = document.querySelector(".modal-overlay");
const closeLoginModal = document.querySelector(".modal-login .modal-close");

const showLoginModal = evt => {
  evt.preventDefault();
  loginModal.classList.add("modal-show");
  overlayModal.classList.add("modal-show");
}

const hideLoginModal = () => {
  loginModal.classList.remove("modal-show");
  overlayModal.classList.remove("modal-show");
}

loginLink.addEventListener("click", showLoginModal);
closeLoginModal.addEventListener("click", hideLoginModal);